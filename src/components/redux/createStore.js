import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import createSagaMiddle from 'redux-saga';

import rootReducer from './rootReducer';


const sagaMiddleware = createSagaMiddle();
export const middlewares = [thunk,sagaMiddleware,logger];

export const store = createStore(rootReducer, applyMiddleware(...middlewares));
sagaMiddleware.run();

export default store;
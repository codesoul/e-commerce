import React, { useState, useEffect } from 'react';
import {useDispatch , useSelector} from 'react-redux';
import { withRouter } from 'react-router-dom';
import { signUpUser , resetAllAuthForms} from './../redux/User/user.actions';
import './styles.scss';



import AuthWrapper from './../AuthWrapper';
import FormInput from './../forms/FormInput';
import Button from './../forms/Button';

// const initialState = {
//     displayName: '',
//     email: '',
//     password: '',
//     confirmPassword: '',
//     errors: [],
// }

const mapState = ({user}) => ({
    signUpSuccess : user.signUpSuccess,
    signUpError: user.signUpError
})


//class Signup extends Component {
const Signup = props =>{

    const { signUpSuccess, signUpError} = useSelector(mapState);
    const dispatch = useDispatch();
    const [displayName, setDisplayName] = useState(''); 
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [errors, setErrors] = useState([]);
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         ...initialState
    //     };

    //     this.handleChange = this.handleChange.bind(this);
    // }

    useEffect(()=>{
        if(signUpSuccess){
            reset();
            dispatch(resetAllAuthForms());
            props.history.push('/');
        }
    }, [signUpSuccess]);

    
    useEffect(()=>{
        if(Array.isArray(signUpError) && signUpError.length > 0){
            setErrors(signUpError);
        }

    }, [signUpError]);


    // handleChange(e) {
    //     const { name, value } = e.target;

    //     this.setState({
    //         [name]: value
    //     })
    // }

    const reset = () =>{
        setDisplayName('');
        setEmail('');
        setPassword('');
        setConfirmPassword('');
        setErrors([]);
    }


    const handleFormSubmit = event =>{
        event.preventDefault();
        dispatch(signUpUser({
            displayName, email, password, confirmPassword
        }))
    }



    // code without redux
    // const handleFormSubmit = async event => {
    //     event.preventDefault();
    //     //const { displayName, email, password, confirmPassword, errors } = this.state;

    //     if (password !== confirmPassword) {
    //         const err = ['Password dont match'];
    //         // this.setState({
    //         //     errors: err
    //         // })\
    //         setErrors(err);
    //         return;
    //     }

    //     try {
    //         const { user } = await auth.createUserWithEmailAndPassword(email, password);

    //         await handleUserProfile(user, { displayName });

    //         // this.setState({
    //         //     ...initialState
    //         // })
    //         reset();
    //         props.history.push('/');

    //     } catch (e) {

    //     }
    // }

    //render() {
       // const { displayName, email, password, confirmPassword, errors } = this.state;

        const configAuthWrapper = {
            headline : 'Registration'
        }

        return (

            <AuthWrapper {...configAuthWrapper}>
                <div className="formWrap">
                    {errors.length > 0 && (
                        <ul>
                            {errors.map((err, index) => {
                                return (
                                    <li key={index}>
                                        {err}
                                    </li>
                                )
                            })}
                        </ul>
                    )}
                    <form onSubmit={handleFormSubmit}>
                        <FormInput type="text" name="displayName" value={displayName} placeholder="Full Name" handleChange = {e => setDisplayName(e.target.vlaue)} />

                        <FormInput type="email" name="email" value={email} placeholder="Email" handleChange = {e => setEmail(e.target.vlaue)} />

                        <FormInput type="password" name="password" value={password} placeholder="Password" handleChange = {e => setPassword(e.target.vlaue)} />

                        <FormInput type="password" name="confirmPassword" value={confirmPassword} placeholder="Full Name" handleChange = {e => setConfirmPassword(e.target.vlaue)} />

                        <Button type="submit">
                            Register
                        </Button>
                    </form>
                </div>
            </AuthWrapper>
        );
    }
//}

export default withRouter(Signup);
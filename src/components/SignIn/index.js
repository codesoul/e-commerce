//import React, { Component } from 'react';
import React, { useState , useEffect} from 'react';
import { useDispatch , useSelector} from 'react-redux';
import { signInUser, signInWithGoogle, resetAllAuthForms } from './../redux/User/user.actions';

import {Link, withRouter} from 'react-router-dom';
import './styles.scss';
import Buttons from './../forms/Button';
//import { signInWithGoogle, auth } from './../../firebase/utils';

import AuthWrapper from './../AuthWrapper';
import FormInput from './../forms/FormInput';
import Button from './../forms/Button';

// const initialState = {
//     email: '',
//     password:''
// }

//class SignIn extends Component {

const mapState = ({user}) => ({
    signInSuccess: user.signInSuccess
})

const SignIn = props =>{
    const { signInSuccess } = useSelector(mapState);
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         ...initialState
    //     }

    //     this.handleChange = this.handleChange.bind(this);
    // }

    // handleChange(e){
    //     const {name,value} = e.target;

    //     this.setState({
    //         [name] : value
    //     })
    // }

    useEffect(() => {
        if(signInSuccess){
            resetForm();
            dispatch(resetAllAuthForms());
            props.history.push('/');
        }
    }, [signInSuccess])


    const resetForm = () =>{
        setEmail('');
        setPassword('');
    }

    // const handleSubmit = async e =>{
    //     e.preventDefault();
    //     //const {email, password} = this.state;

    //     try{
    //         await auth.signInWithEmailAndPassword(email, password);
    //         resetForm();
    //         props.history.push('/');
    //         // this.setState({
    //         //     ...initialState
    //         // })
           

    //     }catch(e){

    //     }
    // }

    const handleSubmit = e =>{
        e.preventDefault();
        dispatch(signInUser({email,password}));
    }

    const handleGoogleSignIn = () =>{
        dispatch(signInWithGoogle())
    }

    //render() {

       // const { email, password} = this.state;

        const configAuthWrapper = {
            headline: 'LogIn'
        };

        return (
               <AuthWrapper {...configAuthWrapper}>

                    <div class="formWrap">
                        <form onSubmit={handleSubmit}>
                            
                            <FormInput type = "email" name = "email" value = {email} placeholder = "Email" handleChange = {e => setEmail(e.target.value)}/>
                            
                            <FormInput type = "password" name = "password" value = {password} placeholder = "Password" handleChange = {e => setPassword(e.target.value)}/>
                            
                            <Button type = "submit">Login</Button>

                            <div className="socialSignin">
                                <div className="row">
                                    <Buttons onClick={handleGoogleSignIn}>
                                        Sign in with Google
                                </Buttons>
                                </div>
                            </div>

                            <div className = "links"> 
                                <Link to="/recovery">
                                    Reset Password
                                </Link>
                            </div>
                        </form>
                    </div>
                </AuthWrapper>
        )
    }
//}

export default withRouter(SignIn);
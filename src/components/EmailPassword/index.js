import React , {useState, useEffect} from 'react';
import { useDispatch, useSelector} from 'react-redux';
import { withRouter } from 'react-router-dom';
import {resetPassword, resetAllAuthForms} from './../redux/User/user.actions'
import './styles.scss';

import AuthWrapper from './../AuthWrapper';
import FormInput from './../forms/FormInput';
import Button from './../forms/Button';

//import {auth} from '../../firebase/utils';

// const initialState = {
//     email: '',
//     errors: []
// };

const mapState = ({ user }) =>({
    resetPasswordSuccess: user.resetPasswordSuccess,
    resetPasswordError: user.resetPasswordError
})

//class EmailPassword extends Component{
const EmailPassword = props =>{    
    const {resetPasswordSuccess, resetPasswordError} = useSelector(mapState);
    const dispatch = useDispatch();
    const [email,setEmail] = useState('');
    const [errors,setErrors] = useState([]);
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         ...initialState
    //     }
    //     this.handleChange = this.handleChange.bind(this);
    // }

    // handleChange(e){
    //     const {name, value} = e.target;
    //     this.setState({
    //         [name]:value
    //     })
    // }

    useEffect(() => {
        if(resetPasswordSuccess){
            dispatch(resetAllAuthForms());
            props.history.push('/login');
        }
    }, [resetPasswordSuccess]);

    useEffect(() => {
        if(Array.isArray(resetPasswordError) && resetPasswordError.length > 0){
            setErrors(resetPasswordError);
        }
    }, [resetPasswordError]);

    
    const handleSubmit = e =>{
        e.preventDefault();
        dispatch(resetPassword({email}));

    }




    // const handleSubmit = async (e) =>{
    //     e.preventDefault();

    //     try{
    //        // const {email} = this.state;
    //         const config = {
    //             url: 'http://localhost:3000/login'
    //         };
    //         await auth.sendPasswordResetEmail(email,config)
    //             .then(()=>{
    //                 props.history.push('/login');
    //             })
    //             .catch(()=>{
    //                 const err = ['Email not fount. Please try again'];
    //                 // this.setState({
    //                 //     errors: err
    //                 // })
    //                 setErrors(err);
    //             })

    //     }catch(e){

    //     }
    // }


  //  render(){
   //     const {email, errors} = this.state;
        const configAuthWrapper = {
            headline: 'Email Password'
        }

        return(
            <AuthWrapper {...configAuthWrapper}>
                <div className = "formWrap">

                    {errors.length > 0 && (
                        <ul>
                             {errors.map((e,index) =>{
                                 return(
                                     <li key = {index}>
                                         {e}
                                     </li>
                                 )
                             })}
                        </ul>
                    )}

                    <form onSubmit= {handleSubmit}>
                        <FormInput type = "email" name = "email" value = {email} placeholder = "email" handleChange = {e => setEmail(e.target.value)}/>
                        <Button type = "submit">
                            Email Password
                        </Button>
                    </form>
                </div>
            </AuthWrapper>
        );
    }
//}

export default withRouter(EmailPassword);
import React, { useEffect } from 'react';
//import { connect } from 'react-redux';
import { useSelector, useDispatch } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { auth, handleUserProfile } from './firebase/utils';
import { setCurrentUser } from '../src/components/redux/User/user.actions';

//hoc
import WithAuth from './hoc/withAuth';

import './default.scss';

//layouts
import MainLayout from './layouts/MainLayouts';
import HomepageLayout from './layouts/HomoepageLayout';

//pages
import Homepage from './pages/Homepage';
import Registration from './pages/Registration'
import Login from './pages/Login';
import Recovery from './pages/Recovery';
import Dashboard from './pages/Dashboard';


// const initialState = {
//   currentUser: null
// };

//class App extends Component {

const App = props => {

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     ...initialState
  //   }
  // }

  //authListener = null;

  //const { setCurrentUser, currentUser } = props;

  const dispatch = useDispatch();
  useEffect(() => {
    //alert(this.props);


    const authListener = auth.onAuthStateChanged(async userAuth => {

      if (userAuth) {
        const userRef = await handleUserProfile(userAuth);
        userRef.onSnapshot(snapshot => {
          dispatch(setCurrentUser({
            id: snapshot.id,
            ...snapshot.data()
          }))
        })
      }

      dispatch(setCurrentUser(userAuth));


      // this.setState({
      //   currentUser: userAuth
      // });
    });

    return () => {
      authListener();
    }
  }, []);



  //render() {
  // const { currentUser } = this.props;
  return (
    <div className="App">
      {/* <Header />
      <div className="main">
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route path="/registration" component={Registration} />
        </Switch>
      </div> */}
      <Switch>
        <Route exact path="/" render={() =>
          <HomepageLayout >
            <Homepage />
          </HomepageLayout>
        } />
        <Route path="/registration" render={() =>  (
          <MainLayout >
            <Registration />
          </MainLayout>
        )} />
        <Route path="/login"
          render={() =>  (
            <MainLayout >
              <Login />
            </MainLayout>
          )} />
        <Route path="/recovery" render={() => (
          <MainLayout>
            <Recovery />
          </MainLayout>
        )} />

        <Route path="/dashboard" render={() => (
          <WithAuth>
            <MainLayout>
              <Dashboard />
            </MainLayout>
          </WithAuth>
        )} />

      </Switch>
    </div>
  );
}
//}

// const mapStateToProps = ({ user }) => ({
//   currentUser: user.currentUser
// });

// const mapDispatchToProps = dispatch => ({
//   setCurrentUser: user => dispatch(setCurrentUser(user))
// });

//export default connect(mapStateToProps, mapDispatchToProps)(App);
export default App;